# Kiwi weekend

Entry task for the Kiwi.com Weekend in the Cloud

Start custom NGINX container:

```
docker run --name kiwi-weekend -d -p 80:80 -p 443:443 durki/kiwi-weekend
```
